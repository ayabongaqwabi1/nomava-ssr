import Helmet from 'react-helmet';
import React, { PureComponent } from 'react';

class PageSEO extends PureComponent {
    render() {
        const { title, description, image } = this.props;
        return (
            <Helmet
                title={title}
                meta={[
                    { name: 'description', content: description },
                    { property: 'og:type', content: 'website' },
                    { property: 'og:title', content: title },
                    { property: 'og:image', content: image },
                ]}
            />
        );
    }
}

export default PageSEO;
