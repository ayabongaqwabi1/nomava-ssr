import React, { Component } from 'react';
import './style.scss';
import { isMobile, isBrowser } from "react-device-detect";
import moment from 'moment';
import { Button }  from "semantic-ui-react";

import * as R from 'ramda';

export default class Post extends Component {
    render() {
        const { thumbnail, user, title, brief, category, date, side, readPost }= this.props;
        const likePost = ()=>{}
        const isLoggedIn = false;
        const likes = 0
        const likeButtonText = isMobile ? "" : "Like";
        const PostSizes = isBrowser ? "col-xs-12 col-md-6" : "col-xs-12 col-sm-8";
        const postSide = side || 'left';
        return (
            <div className={`post ${postSide}-sided ${PostSizes}`}>
                <div className="image">
                    <img src={thumbnail} alt='thumbnail' />
                </div>
                <div className="introbox">
                    <p className="title">{title}</p>
                    <div className="intro" dangerouslySetInnerHTML={{__html: brief}} />
                    <div className="details">
                        {isBrowser && <p>{category}</p>}
                        {isBrowser && <p>{moment(date).format('DD MMM YYYY')}</p>}
                        {isLoggedIn &&
                            <Button
                                color='red'
                                content={likeButtonText}
                                icon='heart'
                                label={{ basic: true, color: 'red', pointing: 'left', content: R.isNil(likes) ? 0 : likes.length  }}
                                onClick={likePost()}
                            />
                        }
                        <Button color='orange' content='Read' icon='book' labelPosition='right' onClick={() => readPost()} />
                    </div>
                </div>
            </div>
        );
    }
}