import { Component } from 'react';
import axios from 'axios'
import './style.scss';
import Router from 'next/router'
import * as R from 'ramda';
import PostItem from '../components/PostItem';
import african  from '../static/african.svg';
class App extends Component {
 constructor(){
   super();
   this.state = {
     postsRendered:[],
     loadingState: false,
     noMorePosts: false,

   }
 }
  static async getInitialProps() {
    console.log('getting initial props')
    let response = await axios.post('http://localhost:3000/api/posts/getset', {index: 0});
    return { posts: response.data.posts };
  }
  componentWillMount(){
    this.setState({ postsRendered: this.props.posts});
  }
  componentDidMount() {

    this.refs.iScroll.addEventListener("scroll", () => {
      const thirtyPercent = this.refs.iScroll.scrollHeight * 0.085;
      if (this.refs.iScroll.scrollTop + this.refs.iScroll.clientHeight >=(this.refs.iScroll.scrollHeight - thirtyPercent)){
        this.loadMoreItems();
      }
    });
}
  displayItems() {
    var items = [];
    console.log(this.state)
    this.state.postsRendered.map( post =>{
      const { title, brief, category, image, date , content} = post;
      items.push(
        <PostItem 
          title={title} 
          key={title}
          brief={content.brief}
          category={category}
          thumbnail={image.secure_url}
          date={date}
          readPost={() => Router.push(`/post?id=${post._id}`)}
        />);
    })
    return items;
  }
  
  loadMoreItems() {
      this.setState({ loadingState: true, noMorePosts: false });
      axios.post('http://localhost:3000/api/posts/getset', {index:  this.state.postsRendered.length})
        .then(res => {
          const nexPosts = res.data.posts;
          if(R.isEmpty(nexPosts)){
            this.setState({noMorePosts: true, loadingState: false})
          }
          else{
            this.setState({ postsRendered: R.union(this.state.postsRendered, nexPosts), loadingState: false });
          }
        })
  }
  render() {
    return (
        <div className='content' ref='iScroll'>
          {this.displayItems()}           
          <div className="col-md-12 text-center">
            {this.state.loadingState ? <p className="loading"> loading More Items..</p> : ""}
            {this.state.noMorePosts ? <p className="loading"> * no more posts * </p> : ""}
            <img className='floating' src={african} alt="logo"/>
          </div>                     
        </div>
    );
  };
}

export default App;
