import { Component } from 'react';
import axios from 'axios'
import * as R from 'ramda';
import './style.scss';
import nl2br from 'nl2br';
import Link from 'next/link'
import { Button } from 'semantic-ui-react';
import PageSeo from '../../components/PageSeo';
class App extends Component {

  static async getInitialProps({query}) {
    console.log(query)
    const { id } = query;
    let response = await axios.get(`http://localhost:3000/api/post/${id}`);
    return { posts: response.data };
  }

  render() {
    console.log('rendering')
    console.log(this.props.posts)
    const post = R.head(this.props.posts);
    console.log(post)
    const hasOpenPost = !R.isNil(post);
    const { title, content, image} = post;
    return (
      <div className='container'>
        <PageSeo title={title} description={content.brief} image={image.secure_url} />
        <div className='wrapper'>
            <div className="detail-container">
                <div className="detail text-center">
                    <div className="row">
                        <div className="col-md-3"></div>
                        <div className="col-md-6 text-left back-to-posts">
                            <Link href={'/'} passHref>
                                <Button content='Back to Articles' icon='left arrow' labelPosition='left' inverted color='orange'/>
                            </Link>
                        </div>
                    </div>
                    {hasOpenPost &&
                        <div className="col-md-6 col-xs-12 post-text">
                            <div>
                                <h1> {`${nl2br(title)}`}</h1>
                                <img src={image.secure_url} alt='thumbnail' />
                                <br />
                                <div dangerouslySetInnerHTML={{__html: nl2br(content.brief)}} />
                                <br />
                                <div dangerouslySetInnerHTML={{ __html: nl2br(content.extended)}} />
                            </div>
                        </div>
                    }
                    {!hasOpenPost &&
                        <div className="row text-center loader-container">
                            <div className="loader col-md-6 col-xs-12">
                                <div className="loading">
                                    <img src={load} alt="loading"/>
                                </div>
                            </div>
                        </div>
                    }
                </div>
            </div>
        </div>
      </div>
    );
  };
}

export default App;
